#! /bin/sh

# Source message functions from appropriate location
if [ "$0" = util/uninstall.sh ]; then
    . src/msg.sh
else
    . /usr/local/include/vesdk/init/msg.sh
fi

# Removes directory if empty
prune()
{
    if find "$1" -prune -type d -empty 1>/dev/null 2>&1 | grep -q '^'; then
	rm -rf "$1" 1>/dev/null 2>&1
    fi
}

# Ensure we're root
if [ "$(id -u)" -ne 0 ]; then
    vei_msg_fail 'Permission denied; uninstall as root'
fi

# Uninstall library
vei_msg_info 'Removing library'
root=/usr/local/include/vesdk
rm -rf "$root/init" 1>/dev/null 2>&1
prune "$root"

# Uninstall man pages
vei_msg_info 'Removing man pages'
rm -f /usr/local/man/man3/vesdk.init.* 1>/dev/null 2>&1
rm -f /usr/local/man/man8/vesdk.init.* 1>/dev/null 2>&1
makewhatis

# Uninstall supplementary notes
vei_msg_info 'Removing supplementary notes'
root=/usr/local/share/doc/vesdk
rm -rf "$root/init" 1>/dev/null 2>&1
prune "$root"

# Uninstall support utilities
vei_msg_info 'Removing utilities'
rm -f /usr/local/sbin/vesdk-init-uninstall 1>/dev/null 2>&1

# Validate
vei_msg_info 'Validating uninstall'
msg='Uninstall validation failed; you may have to fix manually'
[ -d /usr/local/include/vesdk/init ] && vei_msg_fail "$msg"
[ -d /usr/local/share/doc/vesdk/init ] && vei_msg_fail "$msg"
ls /usr/local/sbin/vesdk-init-* 1>/dev/null 2>&1 &&  vei_msg_fail "$msg"
ls /usr/local/man/man3/vesdk.init.* 1>/dev/null 2>&1 &&  vei_msg_fail "$msg"
# ls /usr/local/man/man7/vesdk.init.* 1>/dev/null 2>&1 && vei_msg_fail "$msg"
ls /usr/local/man/man8/vesdk.init.* 1>/dev/null 2>&1 && vei_msg_fail "$msg"

# Farewell!
vei_msg_ok 'uninstall complete'
