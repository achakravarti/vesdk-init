#! /bin/sh

. src/msg.sh

# Undoes installation if something goes wrong
undo()
{
    if [ -z "$1" ]; then
	vei_msg_warn 'something went wrong, reverting'
    else
	vei_msg_warn "$1"
    fi
    util/uninstall.sh
    vei_msg_fail 'install undone'
}

# Validates two files have same MD5 sum
validate()
{
    msg="MD5 mismatch: $1 -> $2"
    [ -f "$1" ] || undo "$1 not found"
    [ -f "$2" ] || undo "$2 not found"
    [ "$(md5 -q "$1")" = "$(md5 -q "$2")" ] || undo "MD5 mismatch: $1 -> $2"
}

# Don't proceed if we're not root
if [ "$(id -u)" -ne 0 ]; then
    vei_msg_fail 'permission denied; install as root'
fi

# Don't proceed if already installed
msg='installation exists; run make uninstall first'
[ -d /usr/local/include/vesdk/init ] && vei_msg_fail "$msg"
[ -d /usr/local/share/doc/vesdk/init ] && vei_msg_fail "$msg"
ls /usr/local/sbin/vesdk-init-* 1>/dev/null 2>&1 && vei_msg_fail "$msg"
ls /usr/local/man/man3/vesdk.init.* 1>/dev/null 2>&1 && vei_msg_fail "$msg"
# ls /usr/local/man/man7/vesdk.init.* 1>/dev/null 2>&1 && vei_msg_fail "$msg"
ls /usr/local/man/man8/vesdk.init.* 1>/dev/null 2>&1 && vei_msg_fail "$msg"

# Don't proceed if build is incomplete
msg='build incomplete; run make first'
[ -d build/doc ] || vei_msg_fail "$msg"
man=$(find doc/ -type f -name '*.[3-8]' | cut -d '/' -f 2)
for m in $man; do
    [ -f "build/doc/vesdk.init.$m" ] || vei_msg_fail "$msg"
done

# Install library
vei_msg_info 'installing library'
dir=/usr/local/include/vesdk/init
mkdir -p "$dir" || undo
cp src/* "$dir" || undo

# Install man pages
vei_msg_info 'installing man pages'
cp build/doc/*.3 /usr/local/man/man3 || undo
#cp build/doc/*.7 /usr/local/man/man7 || undo
cp build/doc/*.8 /usr/local/man/man8 || undo
makewhatis || undo

# Install supplementary notes
vei_msg_info 'installing supplementary notes'
dir=/usr/local/share/doc/vesdk/init
mkdir -p "$dir" || undo
cp README LICENSE "$dir" || undo

# Install support utilities
vei_msg_info 'installing utilities'
cp util/uninstall.sh /usr/local/sbin/vesdk-init-uninstall || undo

# Validate install
vei_msg_info 'validating install'
src=$(find src/ -type f -name '*.sh' | cut -d '/' -f 2)
for s in $src; do
    validate "src/$s" "/usr/local/include/vesdk/init/$s"
done
man=$(find doc/ -type f -name '*.3' | cut -d '/' -f 2)
for m in $man; do
    validate "build/doc/vesdk.init.$m" "/usr/local/man/man3/vesdk.init.$m"
done
validate util/uninstall.sh /usr/local/sbin/vesdk-init-uninstall
validate README /usr/local/share/doc/vesdk/init/README
validate LICENSE /usr/local/share/doc/vesdk/init/LICENSE

# We're good!
vei_msg_ok 'install complete'
