#!/bin/sh
# -*- mode: sh -*-

# vesdk/init/test/msg.sh -- tests for message component.
# This file is part of the VE SDK Init Library.
# See vesdk.init.msg(3) for documentation.
#
# SDPX-License-Identifier: ISC
# Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.


. "$(dirname "0")/../src/msg.sh"


vei_msg_ok 'Hello, world!'
vei_msg_ok 'Hello, world!' 2>test.log

vei_msg_info 'Hello, world!'
vei_msg_info 'Hello, world!' 2>>test.log

vei_msg_warn "Hello, world!"
vei_msg_warn 'Hello, world!' 2>>test.log

vei_msg_dump 'Hello, world!'
vei_msg_dump 'Hello, world!' 2>>test.log

mount | vei_msg_dump
mount | vei_msg_dump 2>>test.log

vei_msg_fail "Bye, world!"
