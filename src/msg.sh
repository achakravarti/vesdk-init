#! /bin/sh
# -*- mode: sh -*-
#
# vesdk/init/src/msg.sh -- interface for printing messages.
# This file is part of the VE SDK Init Library.
# See vesdk.init.msg(3) for documentation.
#
# SDPX-License-Identifier: ISC
# Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.


# Prints OK message to stderr.
vei_msg_ok()
{
    ts=$(date +'%b %d %H:%M:%S')

    if [ -t 2 ]; then
	printf                                                     \
            '[\033[0;32m OK \033[0m] \033[0;35m%s\033[0m: %s...\n' \
            "$ts"                                                  \
            "$1"                                                   \
            1>&2
    else
	printf '[ OK ] %s: %s...\n' "$ts" "$1" 1>&2
    fi
}


# Prints INFO message to stderr.
vei_msg_info()
{
    ts=$(date +'%b %d %H:%M:%S')

    if [ -t 2 ]; then
	printf                                                     \
            '[\033[0;34mINFO\033[0m] \033[0;35m%s\033[0m: %s...\n' \
            "$ts"                                                  \
	    "$1"                                                   \
	    1>&2
    else
	printf '[INFO] %s: %s...\n' "$ts" "$1" 1>&2
    fi
}


# Prints WARN message to stderr.
vei_msg_warn()
{
    ts=$(date +'%b %d %H:%M:%S')

    if [ -t 2 ]; then
	printf                                                     \
            '[\033[1;33mWARN\033[0m] \033[0;35m%s\033[0m: %s...\n' \
	    "$ts"                                                  \
	    "$1"                                                   \
	    1>&2
    else
	printf '[WARN] %s: %s...\n' "$ts" "$1" 1>&2
    fi
}


# Prints FAIL message to stderr.
vei_msg_fail()
{
    ts=$(date +'%b %d %H:%M:%S')

    if [ -t 2 ]; then
	printf                                                     \
	    '[\033[1;31mFAIL\033[0m] \033[0;35m%s\033[0m: %s...\n' \
	    "$ts"                                                  \
	    "$1"                                                   \
	    1>&2
    else
	printf '[FAIL] %s: %s...\n' "$ts" "$1" 1>&2
    fi

    exit 1
}


# Prints output of command.
# TODO: implementation still buggy
vei_msg_dump()
{
    ts=$(date +'%b %d %H:%M:%S')

    if [ -z "$1" ]; then
	if [ -t 2 ]; then
	    printf '[\033[0;36mDUMP\033[0m] \033[0;35m%s\033[0m:\n' "$ts" 1>&2
	else
	    printf '[DUMP] %s:\n' "$ts" 1>&2
	fi

	cat 1>&2
    else
	if [ -t 2 ]; then
	    printf                                                   \
		'[\033[0;36mDUMP\033[0m] \033[0;35m%s\033[0m:\n%s\n' \
		"$ts"                                                \
		"$1"                                                 \
		1>&2
	else
	    printf '[DUMP] %s:\n%s\n' "$ts" "$1" 1>&2
	fi
    fi
}
