\" -*- mode: mdoc -*-
\"
\" vesdk/init/doc/msg.3 -- man page for message component.
\" This file is part of the VE SDK Init Library.
\" See vesdk.init.msg(3) for documentation.
\"
\" SPDX-License-Identifier: ISC
\" Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
\"
\" THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
\" REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
\" AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
\" INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
\" LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
\" OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
\" PERFORMANCE OF THIS SOFTWARE.
\"


include(`macros.m4')


.Dd October 10, 2022
.Dt VESDK.INIT.MSG 3


.Sh NAME
.Nm vei_msg_ok, vei_log_info, vei_log_warn, vei_log_fail, vei_log_dump
.Nd interface for printing messages
.Os


.Sh SYNOPSIS
.Fd . /usr/local/src/vesdk/init/msg.sh
.Pp
.Sy vei_msg_ok Fa "$1"
.Pp
.Sy vei_msg_info Fa "$1"
.Pp
.Sy vei_msg_warn Fa "$1"
.Pp
.Sy vei_msg_fail Fa "$1"
.Pp
.Sy vei_msg_dump Fa "$1"


.Sh DESCRIPTION
This man page describes in detail the design and use of the message component of
.Xr vesdk.init 7 .
It is meant to be used as the primary reference for both clients and maintainers
of the message component.
.Pp
In shell scripts, it is often useful to provide feedback on what actions are
being performed as a script is running. The message component of
.Xr vesdk.init 7
facilitates this by providing shell functions to print standardised formatted 
messages to
.Em stderr .
All other components of
.Xr vesdk.init 7
depend on this component, so it is necessary to always source it if the
functions from any other component are used, even if the messaging functions are
not used directly.
.Pp
The message component provides the following functions:
.Bl -tag -width Ds
.It Fn vei_msg_ok
Prints a success message.
.It Fn vei_msg_info
Prints an informational message.
.It Fn vei_msg_warn
Prints a warning message.
.It Fn vei_msg_fail
Prints an error message.
.It Fn vei_msg_dump
Prints the output of a command.
.El
.Pp
Each of these functions prints a coloured timestamped message captioned with its
severity. All of these functions print to
.Em stderr
and not to
.Em stdout
in order to allow the message output to separated from the main contextual
output. If required, the message output may be redirected or tee'd to a separate
log file.
.Pp
Additionally,
.Fn vei_msg_fail
also automatically exits the
shell, which is not the case with the other functions. The
.Fn vei_msg_dump
function is special in that the output of any shell command may be piped to it
as the message; this is not possible with the other message functions.
.Pp
The message to be printed is passed through the first parameter of the function,
and the caption of the message is determined by which function is invoked.
Except for
.Fn vei_log_dump ,
the format of the printed message is as follows:
.Bd -literal -offset indent
[caption] timestamp: message...
.Ed
.Pp
In the case of
.Fn vei_log_dump ,
the format is:
.Bd -literal -offset indent
[caption] timestamp:
message
.Ed
.Pp
.Em caption
is determined by which msg function has been invoked, and is one of the
following:
.Bl -bullet -width Ds
.It
[ OK ] (printed in green) if
.Fn vei_msg_ok
has been called
.It
[INFO] (printed in blue) if
.Fn vei_msg_info
has been called
.It
[WARN] (printed in bold orange) if
.Fn vei_msg_warn
has been called
.It
[FAIL] (printed in bold red) if
.Fn vei_msg_fail
has been called
.It
[DUMP] (printed in cyan) if
.Fn vei_msg_dump
has been called
.El
.Pp
The caption is always enclosed in square brackets, and the OK caption has a
padding of 1 whitespace character around it to keep its width to 4 characters
consistent with the other captions.
.Pp
The timestamp is in the Linux kernel msg format, which in the
.Em strftime
format is
.Ql %b %d %H:%M:%S .
The timestamp is printed in purple after the caption by all of the message
functions.
.Pp
.Em message
is the contextual message that is to be printed and is passed to each of the
message functions through their first (and only) parameter as an unformatted
string. Ellipses are automatically added to the end of the message, except in
the case of
.Fn vei_msg_dump .
.Pp
For concrete examples, see the sample outputs listed in the
.Sx EXAMPLES
section of this page.
.El

.Ss Technical details
The implementation of each of the message functions follows a similar pattern as
listed below.
.Bl -enum -idth Ds
.It
Determines the current timestamp in the format as described above.
.It
Prints the message to
.Em stderr
with the required colour codes as described above if
.Em stderr
is connected to the terminal.
.It
If
.Em stderr
is not connected to the terminal, then the message is printed in the same format
but without the colour codes.
.El
.Pp
In addition to these steps, there is an extra bit of processing for
.Fn vei_msg_dump
so that it can handle the piping of the output of other commands. It does so by
checking whether it has received any argument to its first parameter; if not,
then it assumes that the output of an external command is being piped to it and
runs the
.Xr cat 1
utility to print the output of the external command to
.Em stderr .
.Pp
The
.Xr vesdk.init.msg 3
man page is generated from its corresponding template in the source tree
.Po
see
.Sx FILES
.Pc
.Ns .
The template contains the documentation in
.Xr mdoc 7
format interleaved with
.Xr m4 1
macros
.Pq from the documentation macros file
abstracting reusable content common to other
.Xr vesdk.init 7
man pages. The template also uses whitespace to improve readability. During the
build process defined by
.Xr vesdk.init.makefile 5
these macros are expanded and the whitespace is stripped off.


.Sh FILES
This section describes the files related to the message component in both the
source tree (denoted by $SRCTREE) and the host system where
.Xr vesdk.init 7
is installed.
.Pp
After installation, the following files are available on the host system:
.Bl -tag -width Ds
.It Pa /usr/local/src/vesdk/init/msg.sh
Implementation of the message component; this is a copy of
.Pa $SRCTREE/src/msg.sh .
.It Pa /usr/local/man/man3/vesdk.init.msg.3
Man page for vesdk.init.msg(3); this is generated from
.Pa $SRCTREE/doc/msg.3 .
.It Pa /usr/local/share/examples/vesdk/init/msg.sh
Detailed examples of usage; this is a copy of
.Pa $SRCTREE/examples/msg.sh .
.El
.Pp
Additionally, the following files are in the source tree but are not installed
on the host system:
.Bl -tag -width Ds
.It Pa $SRCTREE/doc/msg.3
The template with embedded m4 macros used to generate the
.Xr vesdk.init.msg 3
man page.
.It Pa $SRCTREE/doc/macros.m4
The m4 macros defining common reusable documentation snippets.
.It Pa $SRCTREE/test/msg.sh
Unit tests for the msg component.
.El


.Sh EXAMPLES
.Bl -bullet -width Ds
.It
To print an OK message:
.Dl vei_msg_ok 'This is an OK message'
.Pp
Sample output:
.Dl [ OK ] Aug 03 09:12:33: This is an OK message...
.It
To print a WARN message and log it to the
.Pa messages.log
file:
.Dl vei_msg_warn 'This is a warning message' 2>messages.log
.Ed
.Pp
Sample output:
.Dl [WARN] Aug 03 09:12:33: This is a warning message...
.It
To print the output of the mount command:
.Dl mount | veil_msg_dump
.Pp
Sample output:
.Bd -literal -offset indent
[DUMP] Oct 12 14:36:47:
/dev/sd0a on / type ffs (local)
/dev/sd0k on /home type ffs (local, nodev, nosuid)
/dev/sd0d on /tmp type ffs (local, nodev, nosuid)
/dev/sd0f on /usr type ffs (local, nodev)
/dev/sd0g on /usr/X11R6 type ffs (local, nodev)
/dev/sd0h on /usr/local type ffs (local, nodev, wxallowed)
/dev/sd0j on /usr/obj type ffs (local, nodev, nosuid)
/dev/sd0i on /usr/src type ffs (local, nodev, nosuid)
/dev/sd0e on /var type ffs (local, nodev, nosuid)
.Ed
.El
.Pp
More examples of common use cases can found in the examples file provided with
the source code (listed in the
.Sx FILES
section).


.Sh SEE ALSO
.Xr vesdk.init 7 ,
.Xr vesdk.init.configure 8
.Sh STANDARDS
All functions of the message component conform to the
.St -p1003.1-2008 Shell Command Language.


.Sh HISTORY
__history__


.Sh AUTHORS
__authors__


.Sh CAVEATS
If
.Fn vei_msg_dump
is invoked without piping the output of an external command to it and without an
argument to its first parameter, it will enter into an infinite loop.


.Sh BUGS
Piping to
.Fn vei_msg_dump
does not work as anticipated in some situations; this is still being
investigated. To report other bugs, please send an e-mail to
.An Abhishek Chakravarti
.Aq Mt abhishek@taranjali.org
mentioning all relevant details.
