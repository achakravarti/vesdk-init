\" -*- mode: mdoc -*-
\"
\" vesdk/init/doc/uninstall.8 -- man page for uninstall utility.
\" This file is part of the VE SDK Init Library.
\" See vesdk.init.uninstall(8) for documentation.
\"
\" SPDX-License-Identifier: ISC
\" Copyright (c) 2022 Abhishek Chakravarti <abhishek@taranjali.org>
\"
\" THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
\" REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
\" AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
\" INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
\" LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
\" OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
\" PERFORMANCE OF THIS SOFTWARE.
\"


include(`macros.m4')


.Dd October 10, 2022
.Dt VESDK.INIT.UNINSTALL 8


.Sh NAME
.Nm vesdk-init-uninstall
.Nd utility for uninstalling the VE SDK Init Library
.Os


.Sh SYNOPSIS
.Sy [doas] vesdk-init-uninstall
.Pp
.Sy [doas] make uninstall


.Sh DESCRIPTION
This man page describes in detail the design and use of the
.Xr vesdk.init 7
uninstall utility. It is meant to be used as the primary reference for both
clients and maintainers of the uninstall utility.
.Pp
The uninstall utility is responsible for safely removing the VE SDK Init Library
in an idempotent manner from the host system where it has been installed by
.Xr vesdk.init.install 8 .
The uninstall utility may be invoked directly by calling
.Sy vesdk-init-uninstall
from the command line; note that root privileges are required, and so the
utility must be run either from the root account or using
.Xr doas 1 .
.Pp
Additionally, the uninstall utility may be invoked indirectly by running the
.Sy uninstall
target of
.Xr vesdk.init.makefile 5 .
Although this option is mainly for the convenience of developers, it may also be
used in the unlikely situation when
.Sy vesdk-init-uninstall
is either not working or unavailable.
.Pp
Note that running the uninstall utility does not remove the cloned Git
repository source tree of
.Xr vesdk.init 7 ;
this must be done manually.

.Ss Technical details
The uninstall utility performs the following steps in sequence.
.Bl -enum -width Ds
.It
Sources
.Xr vesdk.init.msg 3
from the appropriate location depending on how the uninstall utility has been
invoked.
.Xr vesdk.init.msg 3
is used by the uninstall utility for displaying informational, success and
failure messages during execution. If the utility has been invoked directly from
the command line, then it sources
.Xr vesdk.init.msg 3
from the standard install location. If it has been invoked indirectly by running
the
.Sy uninstall
target of
.Xr vesdk.init.makefile 5 ,
then
.Xr vesdk.init.msg 3
is sourced from the source tree itself. This check is possible since the
.Sy uninstall
target of
.Xr vesdk.init.makefile 5
invokes the utility by its source tree file path and not its standard install
path.
.It
Checks whether the utility is being run as root; if not, then the utility aborts
after informing the user to run with root privileges. Root privileges are
required since changes will be made to the
.Pa /usr/local
subdirectories.
.It
Removes the
.Pa /usr/local/include/vesdk/init
directory where the
.Xr vesdk.init 7
shell functions are installed. After removing this subdirectory the
.Pa /usr/local/include/vesdk
directory will become orphaned if there are no other VE SDK components
installed; in such a case it is necessary to remove it too. This is handled by
the
.Fn prune
helper function, which checks whether the directory passed through its first
parameter exists and is empty, and if so, proceeds to delete it.
.It
Removes the VE SDK Init library man pages from the
.Pa /usr/local/man/man3 ,
.Pa /usr/local/man/man7
and
.Pa /usr/local/man/man8
directories. This is easy to do since the man pages follow the glob pattern
.Ql vesdk.init.* .
The man page database is then updated through
.Xr makewhatis 8 .
.It
Removes the
.Pa /usr/local/share/doc/vesdk/init
directory where the
.Pa README
and
.Pa LICENSE
files have been installed. The
.Pa /usr/local/share/doc/vesdk
directory is now pruned if required through the
.Fn prune
function.
.It
Removes the
.Sy vesdk-init-uninstall
utility from
.Pa /usr/local/sbin ;
this does not affect the uninstall process since the utility has already been
loaded into memory.
.It
Validates that all installed files have been removed. If any such files remain,
then validation has failed and a failure message is printed. Otherwise,
validation has succeeded and a success message is printed.
.El
.Pp
The
.Xr vesdk.init.uninstall 8
man page is generated from its corresponding template in the source tree
.Po
see
.Sx FILES
.Pc
.Ns .
The template contains the documentation in
.Xr mdoc 7
format interleaved with
.Xr m4 1
macros
.Pq from the documentation macros file
abstracting reusable content common to other
.Xr vesdk.init 7
man pages. The template also uses whitespace to improve readability. During the
build process defined by
.Xr vesdk.init.makefile 5
these macros are expanded and the whitespace is stripped off.


.Sh FILES
After installation by
.Xr vesdk.init.install 8 ,
the following files are available on the host system:
.Bl -tag -width Ds
.It Pa /usr/local/sbin/vesdk-init-uninstall
Uninstall utility; this is a copy of
.Pa $SRCTREE/util/uninstall.sh ,
where
.Pa $SRCTREE
is the path to the source code tree (the cloned Git repository).
.It Pa /usr/local/man/man8/vesdk.init.uninstall.8
Man page for vesdk.init.uninstall(8); this is generated from the template in
.Pa $SRCTREE/doc/uninstall.8
in conjunction with
.Pa $SRCTREE/doc/macros.m4 .


.Sh EXIT STATUS
.Ex -std vesdk-init-uninstall
See
.Sx DIAGNOSTICS
for more details on errors.


.Sh EXAMPLES
.Bl -bullet -width Ds
.It
To uninstall as root from any location in the command line:
.Dl # vesdk-init-uninstall
.It
To uninstall as non-root user from any location in the command line:
.Dl $ doas vesdk-init-uninstall
.It
To uninstall from within the cloned Git repository source tree:
.Bd -literal -offset indent
$ cd /path/to/source/tree
$ doas make uninstall
.Ed
.El


.Sh DIAGNOSTICS
Any error results in an exit code of 1, supported by one of the following
failure diagnostic messages.
.Bl -diag
.It "Permission denied; uninstall as root"
The uninstall utility has attempted to run without root privileges; this can
be fixed by running the utility again either as a root user or using
.Xr doas 1 .
.It "Uninstall validation failed; you may have to fix manually"
This occurs in the unlikely event that the uninstall utility failed to remove
the installed files. Try running the uninstall utility again by invoking the
.Sy uninstall
target of
.Xr vesdk.init.makefile 5 .
.El


.Sh SEE ALSO
.Xr vesdk.init.makefile 5 ,
.Xr vesdk.init 7 ,
.Xr vesdk.init.install 8


.Sh STANDARDS
.St -p1003.1-2008


.Sh HISTORY
__history__


.Sh AUTHORS
__authors__


.Sh BUGS
__bugs__
